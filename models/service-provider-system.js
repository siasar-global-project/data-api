module.exports = (sequelize, DataTypes) => {
  const ServiceProviderSystem = sequelize.define('ServiceProviderSystem', {
    serviceProviderId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    systemId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    servedHouseholds: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  }, {
    underscored: true,
  });
  return ServiceProviderSystem;
};

module.exports = (sequelize, DataTypes) => {
  const ServiceProvider = sequelize.define('ServiceProvider', {
    siasarId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    surveyDate: DataTypes.DATEONLY,
    surveyYear: DataTypes.INTEGER,
    surveyMonth: DataTypes.INTEGER,
    country: DataTypes.STRING(2),
    adm0: {
      field: 'adm_0',
      type: DataTypes.STRING,
    },
    adm1: {
      field: 'adm_1',
      type: DataTypes.STRING,
    },
    adm2: {
      field: 'adm_2',
      type: DataTypes.STRING,
    },
    adm3: {
      field: 'adm_3',
      type: DataTypes.STRING,
    },
    adm4: {
      field: 'adm_4',
      type: DataTypes.STRING,
    },
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
    geom: DataTypes.GEOMETRY('POINT', 4326),
    score: DataTypes.STRING(1),
    siasarVersion: DataTypes.STRING,
    pictureUrl: DataTypes.STRING,
    servedHouseholds: DataTypes.INTEGER,
    monthBilling: DataTypes.FLOAT,
    legalStatus: DataTypes.BOOLEAN,
    providerType: DataTypes.STRING,
    womenCount: DataTypes.INTEGER,
    sep: DataTypes.FLOAT,
    sepOrg: DataTypes.FLOAT,
    sepOpm: DataTypes.FLOAT,
    sepEco: DataTypes.FLOAT,
    sepEnv: DataTypes.FLOAT,
    prof: DataTypes.FLOAT,
    amsp: DataTypes.FLOAT,
    opefsp: DataTypes.FLOAT,
    genfsp: DataTypes.FLOAT,
    ramafps: DataTypes.FLOAT,
    omf: DataTypes.FLOAT,
    resclf: DataTypes.FLOAT,
    regf: DataTypes.FLOAT,
    microf: DataTypes.FLOAT,
    berf: DataTypes.FLOAT,
    cerf: DataTypes.FLOAT,
    pro: DataTypes.FLOAT,
    lrf: DataTypes.FLOAT,
    srf: DataTypes.FLOAT,
    dscrf: DataTypes.FLOAT,
    espf: DataTypes.FLOAT,
    paaf: DataTypes.FLOAT,
    profitable: DataTypes.BOOLEAN,
    preventiveMaintenance: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    location: {
      type: DataTypes.VIRTUAL,
      get() {
        return [this.get('adm1'), this.get('adm2'), this.get('adm3'), this.get('adm4')];
      },
    },
  }, {
    underscored: true,
  });

  ServiceProvider.associate = function(models) {
    models.ServiceProvider.belongsToMany(models.Community, {
      through: models.ServiceProviderCommunity,
      foreignKey: 'service_provider_id',
      otherKey: 'community_id',
      as: 'communities',
    });
    models.ServiceProvider.belongsToMany(models.System, {
      through: models.ServiceProviderSystem,
      foreignKey: 'service_provider_id',
      otherKey: 'system_id',
      as: 'systems',
    });
  };

  ServiceProvider.beforeFind((query) => {
    if (query.attributes) {
      if (Array.isArray(query.attributes)) {
        if (query.attributes.includes('location')) {
          query.attributes.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      } else if (Array.isArray(query.attributes.include)) {
        if (query.attributes.includes('location')) {
          query.attributes.include.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      }
    }
  });

  return ServiceProvider;
};

module.exports = (sequelize, DataTypes) => {
  const TechnicalProvider = sequelize.define('TechnicalProvider', {
    siasarId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    surveyDate: DataTypes.DATEONLY,
    surveyYear: DataTypes.INTEGER,
    surveyMonth: DataTypes.INTEGER,
    country: DataTypes.STRING(2),
    adm0: {
      field: 'adm_0',
      type: DataTypes.STRING,
    },
    adm1: {
      field: 'adm_1',
      type: DataTypes.STRING,
    },
    adm2: {
      field: 'adm_2',
      type: DataTypes.STRING,
    },
    adm3: {
      field: 'adm_3',
      type: DataTypes.STRING,
    },
    adm4: {
      field: 'adm_4',
      type: DataTypes.STRING,
    },
    longitude: DataTypes.DOUBLE,
    latitude: DataTypes.DOUBLE,
    geom: DataTypes.GEOMETRY('POINT', 4326),
    score: DataTypes.STRING(1),
    siasarVersion: DataTypes.STRING,
    pictureUrl: DataTypes.STRING,
    servedHouseholds: DataTypes.INTEGER,
    tap: DataTypes.FLOAT,
    tapIs: DataTypes.FLOAT,
    tapIca: DataTypes.FLOAT,
    tapCco: DataTypes.FLOAT,
    tapAin: DataTypes.FLOAT,
    providerType: DataTypes.STRING,
    transportEquipment: DataTypes.STRING(1),
    equipmentMeasuring: DataTypes.STRING(1),
    computerEquipment: DataTypes.STRING(1),
    travelExpenses: DataTypes.STRING(1),
    fuel: DataTypes.STRING(1),
    internetServices: DataTypes.STRING(1),
    techniciansCount: DataTypes.INTEGER,
    annualBudget: DataTypes.BOOLEAN,
    budget: DataTypes.JSON,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    location: {
      type: DataTypes.VIRTUAL,
      get() {
        return [this.get('adm1'), this.get('adm2'), this.get('adm3'), this.get('adm4')];
      },
    },
  }, {
    underscored: true,
  });

  TechnicalProvider.associate = function(models) {
    TechnicalProvider.belongsToMany(models.Community, {
      through: models.TechnicalProviderCommunity,
      foreignKey: 'technical_provider_id',
      otherKey: 'community_id',
      as: 'communities',
    });
  };

  TechnicalProvider.beforeFind((query) => {
    if (query.attributes) {
      if (Array.isArray(query.attributes)) {
        if (query.attributes.includes('location')) {
          query.attributes.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      } else if (Array.isArray(query.attributes.include)) {
        if (query.attributes.includes('location')) {
          query.attributes.include.push('adm1', 'adm2', 'adm3', 'adm4');
        }
      }
    }
  });

  return TechnicalProvider;
};

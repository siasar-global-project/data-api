import express from 'express';

import models from '../models';
import commonRoutes from './common-routes';

const router = express.Router();

commonRoutes(router, models.HealthCenter);

router.get('/stats', async (req, res) => {
  res.send({
    healthCenters: await models.HealthCenter.count({
      where: {
        score: { [models.sequelize.Op.notIn]: ['N'] },
        ...req.query.where,
      },
    }),
    staff: await models.HealthCenter.sum('staff', req.query),
    withWaterService: await models.HealthCenter.count({
      where: {
        haveSystem: 'yes',
        ...req.query.where,
      },
    }),
    sufficientWashingFacilities: await models.HealthCenter.count({
      where: {
        sufficientWashingFacilities: 'yes',
        ...req.query.where,
      },
    }),
  });
});

router.get('/:id', async (req, res) => {
  res.send(await models.HealthCenter.findById(req.params.id, {
    include: [{
      model: models.Community,
      as: 'communities',
      attributes: ['siasarId', 'name', 'score'],
    }, {
      model: models.System,
      as: 'systems',
      attributes: ['siasarId', 'name', 'score'],
    }],
  }));
});

export default router;

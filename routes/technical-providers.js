import express from 'express';

import models from '../models';
import commonRoutes from './common-routes';

const router = express.Router();

commonRoutes(router, models.TechnicalProvider);

router.get('/:id', async (req, res) => {
  res.send(await models.TechnicalProvider.findById(req.params.id, {
    include: [{
      model: models.Community,
      as: 'communities',
      attributes: ['siasarId', 'name', 'score'],
      through: { as: 'join', attributes: ['servedHouseholds'] },
    }],
  }));
});

export default router;

import express from 'express';

import models from '../models';
import commonRoutes from './common-routes';

const router = express.Router();

commonRoutes(router, models.School);

router.get('/stats', async (req, res) => {
  res.send({
    schools: await models.School.count({
      where: {
        score: { [models.sequelize.Op.notIn]: ['N'] },
        ...req.query.where,
      },
    }),
    students: await models.School.sum('students', req.query),
    withWaterService: await models.School.count({
      where: {
        haveSystem: 'yes',
        ...req.query.where,
      },
    }),
    sufficientWashingFacilities: await models.School.count({
      where: {
        sufficientWashingFacilities: 'yes',
        ...req.query.where,
      },
    }),
  });
});

router.get('/:id', async (req, res) => {
  res.send(await models.School.findById(req.params.id, {
    include: [{
      model: models.Community,
      as: 'communities',
      attributes: ['siasarId', 'name', 'score'],
    }, {
      model: models.System,
      as: 'systems',
      attributes: ['siasarId', 'name', 'score'],
    }],
  }));
});

export default router;
